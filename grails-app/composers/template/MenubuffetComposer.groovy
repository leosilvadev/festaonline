package template

import entidades.Usuario
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.select.annotation.Listen
import seguranca.AutenticacaoAdmin
import util.composers.AppComposer


class MenubuffetComposer extends AppComposer {

    def afterCompose = { window ->
        Usuario usuario = Executions.current.session.getAttribute(AutenticacaoAdmin.USUARIO_LOGADO)
        $('#compNomeBuffet').val(usuario.buffet.nome)
    }

    @Listen("onClick = #btnSair")
    def sairDoSistema(){
        session.attributes.remove(AutenticacaoAdmin.USUARIO_LOGADO)
        redirect(uri:"/login.zul")
    }

}

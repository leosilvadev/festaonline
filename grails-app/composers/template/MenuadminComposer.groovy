package template

import org.zkoss.zk.ui.select.annotation.Listen
import seguranca.AutenticacaoAdmin


class MenuadminComposer extends zk.grails.Composer {

    def afterCompose = { window ->
        // initialize components here
    }

    @Listen("onClick = #btnSair")
    def sairDoSistema(){
        session.attributes.remove(AutenticacaoAdmin.USUARIO_LOGADO)
        redirect(uri:"/login.zul")
    }

}

package festa

import entidades.Festa
import entidades.Usuario
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Listbox
import org.zkoss.zul.Window
import seguranca.AutenticacaoAdmin
import util.composers.AppComposer


class ListarComposer extends AppComposer {

    @Wire Listbox lstFestas

    def festaService

    def afterCompose = { window ->
        Usuario usuario = Executions.current.session.getAttribute(AutenticacaoAdmin.USUARIO_LOGADO)

        adicionarBuffets Festa.findAllByBuffet(usuario.buffet), lstFestas
    }


    @Listen("onClick = #btnCadastro")
    def abrirModalCadastro(){
        Window window = (Window) Executions.createComponents("cadastro.zul", null, null)
        window.doModal()
    }

    def adicionarBuffets(festas, listBox){
        listBox.append {
            festas.each { Festa festa ->
                listitem(value: festa, onDoubleClick: { this.selecionar festa.id }){ item ->
                    listcell(label:festa.datahora, class: "centralizado")
                    listcell(label:festa.cliente.email)
                    listcell(label:festa.cliente.nome)
                    listcell(label:festa.aniversariante)
                    listcell(label:festa.cliente.telefone)
                    listcell(label: "", class: "centralizado"){
                        span(class: "btn btn-info glyphicon glyphicon-pencil", onClick: { event ->
                            this.selecionar festa.id
                        })
                    }
                    listcell(label: "", class: "centralizado"){
                        span(class: "btn btn-danger glyphicon glyphicon-trash", onClick: { event ->
                            this.remover festa.id
                        })
                    }
                }
            }
        }
    }

    def selecionar(long id){
        Window window = (Window) Executions.createComponents("cadastro.zul", null, [id:id])
        window.doModal()
    }
    def remover(long id){
        def eventoRemover = { evento ->
            if (evento.getName().equals("onYes")) {
                festaService.remover(id)
                redirect(uri:"/festa/listar.zul")

            }
        } as org.zkoss.zk.ui.event.EventListener

        confirmar("Tem certeza que deseja remover esta festa?", eventoRemover)
    }

}

package festa

import entidades.Buffet
import entidades.Festa
import entidades.Usuario
import enums.TipoUsuario
import excecoes.ValidacaoException
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Datebox
import util.composers.AppComposer


class CadastroComposer extends AppComposer {

    def festaService

    private Festa festaAlteracao

    @Listen("onOK = #compDatahora, #compCliente, #compEmail, #compTelefone")
    def aoClicarEnter(){
        salvar()
    }

    @Listen("onCancel = #compDatahora, #compCliente, #compEmail, #compTelefone")
    def aoClicarEsc(){
        fechar()
    }

    def afterCompose = { window ->
        if(arg.id){
            festaAlteracao = Festa.findById(arg.id)
            $('#compDatahora').val festaAlteracao.datahora
            $('#compEmail').val festaAlteracao.cliente.email
            $('#compTelefone').val festaAlteracao.cliente.telefone
            $('#compCliente').val festaAlteracao.cliente.nome
            $('#compAniversariante').val festaAlteracao.aniversariante

        }else{
            $('#compDatahora').val new Date()

        }
    }

    @Listen("onClick = #btnCancelar")
    def fechar(){
        $('#modalCadastro').detach()
    }

    @Listen("onClick = #btnCadastrar")
    def salvar(){
        try{
            String email = $('#compEmail').val()
            Date dataFesta = $('#compDatahora').val()
            String telefone = $('#compTelefone').val()
            String nomeCliente = $('#compCliente').val()
            String aniversariante = $('#compAniversariante').val()

            if(festaAlteracao){
                festaAlteracao.cliente.email = email
                festaAlteracao.cliente.nome = nomeCliente
                festaAlteracao.cliente.telefone = telefone
                festaAlteracao.datahora = dataFesta
                festaAlteracao.aniversariante = aniversariante
                festaService.atualizar festaAlteracao

            }else{
                Usuario cliente = new Usuario(email: email, tipoUsuario: TipoUsuario.CLIENT, telefone: telefone, nome: nomeCliente)
                Festa festa = new Festa(buffet: usuarioLogado().buffet, datahora: dataFesta, aniversariante: aniversariante)

                festaService.salvar festa, cliente, urlAplicacao()

            }
            redirect(uri:"/festa/listar.zul")

        }catch(ValidacaoException ex){
            alertar ex.fieldError

        }
    }

}

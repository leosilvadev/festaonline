package buffet

import entidades.Buffet
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.event.Event
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Listbox
import org.zkoss.zul.Window
import util.composers.AppComposer


class ListarComposer extends AppComposer {

    def buffetService

    @Wire Listbox lstBuffets

    def afterCompose = { window ->
        adicionarBuffets Buffet.findAll(), lstBuffets
    }

    @Listen("onClick = #btnCadastro")
    def abrirModalCadastro(){
        Window window = (Window) Executions.createComponents("cadastro.zul", null, null)
        window.doModal()
    }

    def adicionarBuffets(buffets, listBox){
        listBox.append {
            buffets.each { Buffet buffet ->
                listitem(value: buffet, onDoubleClick: { this.selecionar buffet.id }){ item ->
                    listcell(label:buffet.nome)
                    listcell(label:buffet.endereco)
                    listcell(label:buffet.usuario.email)
                    listcell(label:buffet.telefone, class: "centralizado")
                    listcell(label: "", class: "centralizado"){
                        span(class: "btn btn-info glyphicon glyphicon-pencil", onClick: { event ->
                            this.selecionar buffet.id
                        })
                    }
                    listcell(label: "", class: "centralizado"){
                        span(class: "btn btn-danger glyphicon glyphicon-trash", onClick: { event ->
                            this.remover buffet.id
                        })
                    }
                }
            }
        }
    }

    def selecionar(long id){
        Window window = (Window) Executions.createComponents("cadastro.zul", null, [id:id])
        window.doModal()
    }

    def remover(long id){
        def eventoRemover = { evento ->
            if (evento.getName().equals("onYes")) {
                buffetService.remover(id)
                redirect(uri:"/buffet/listar.zul")

            }
        } as org.zkoss.zk.ui.event.EventListener

        confirmar("Tem certeza que deseja remover este buffet?", eventoRemover)
    }

}

package buffet

import entidades.Buffet
import entidades.Usuario
import enums.TipoUsuario
import excecoes.ValidacaoException
import org.zkoss.zk.ui.WrongValueException
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Textbox
import org.zkoss.zul.Window
import util.composers.AppComposer

class CadastroComposer extends AppComposer {

    def buffetService

    private Buffet buffetAlteracao

    @Listen("onOK = #compNome, #compEmail, #compEndereco, #compTelefone")
    def aoClicarEnter(){
        salvar()
    }

    @Listen("onCancel = #compNome, #compEmail, #compEndereco, #compTelefone")
    def aoClicarEsc(){
        fechar()
    }

    def afterCompose = { window ->
        if(arg.id){
            buffetAlteracao = Buffet.findById(arg.id)
            $('#compNome').val(buffetAlteracao.nome)
            $('#compEmail').val(buffetAlteracao.usuario.email)
            $('#compEndereco').val(buffetAlteracao.endereco)
            $('#compTelefone').val(buffetAlteracao.telefone)

        }else{
            buffetAlteracao=null

        }
    }

    @Listen("onClick = #btnCancelar")
    def fechar(){
        $('#modalCadastro').detach()
    }

    @Listen("onClick = #btnCadastrar")
    def salvar(){
        try{
            if(buffetAlteracao){
                buffetAlteracao.nome = $('#compNome').val()
                buffetAlteracao.usuario.email = $('#compEmail').val()
                buffetAlteracao.endereco = $('#compEndereco').val()
                buffetAlteracao.telefone = $('#compTelefone').val()
                buffetService.atualizar(buffetAlteracao)

            }else{
                Usuario usuario = new Usuario(
                        email: $('#compEmail').val(),
                        tipoUsuario: TipoUsuario.BUFFET
                )

                Buffet buffet = new Buffet(
                        nome: $('#compNome').val(),
                        endereco: $('#compEndereco').val(),
                        telefone: $('#compTelefone').val()
                )

                buffetService.salvar(buffet, usuario, urlAplicacao())
            }
            redirect(uri:"/buffet/listar.zul")

        }catch(ValidacaoException ex){
            alertar ex.fieldError

        }
    }

}

package festaonline

import entidades.Buffet
import entidades.Usuario
import enums.TipoUsuario
import org.zkoss.zhtml.Input
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.WrongValueException
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zk.ui.util.Clients
import org.zkoss.zul.Div
import org.zkoss.zul.Label
import org.zkoss.zul.Textbox
import org.zkoss.zul.Window
import seguranca.AutenticacaoAdmin
import util.composers.AppComposer


class LoginComposer extends AppComposer {

    @Wire Textbox compEmail
    @Wire Textbox compSenha

    @Wire Div divMensagem
    @Wire Label mensagem

    def afterCompose = { Window window ->
        compEmail.focus()
    }

    @Listen("onOK = #compEmail, #compSenha")
    def clicarEnterDentroDeEmail(){
        login()
    }

    @Listen("onClick = #btnEntrar")
    def login(){
        String email = compEmail.value
        String senha = compSenha.value

        if(!email) {
            alertarPorComponent compEmail
            compEmail.focus()
        }
        if(!senha) {
            alertarPorComponent compSenha
            compSenha.focus()
        }

        Clients.showBusy("Fazendo login...")

        Usuario usuario = Usuario.findByEmailAndSenha(email, senha.encodeAsSHA256())

        if(usuario){
            session.attributes.put(AutenticacaoAdmin.USUARIO_LOGADO, usuario)
            redirect(uri: usuario.tipoUsuario.getPaginaPrincipal())

        }else{
            mensagem.value = "E-mail e/ou senha incorreto!"
            compSenha.value = ""
            compSenha
            divMensagem.visible = true
            Clients.clearBusy()

        }
    }
}

package festaonline

import entidades.Buffet
import org.zkoss.zk.ui.WrongValueException
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Textbox
import util.composers.AppComposer


class ResetarSenhaComposer extends AppComposer {

    def buffetService

    @Wire Textbox compSenhaAtual
    @Wire Textbox compNovaSenha
    @Wire Textbox compConfirmaNovaSenha

    def afterCompose = { window ->
    }

    @Listen("onOK = #compSenhaAtual")
    def clicarEnterDetroDeSenhaAtual(){
        resetarSenha()
    }

    @Listen("onOK = #compNovaSenha")
    def clicarEnterDetroDeNovaSenha(){
        resetarSenha()
    }

    @Listen("onOK = #compConfirmaNovaSenha")
    def clicarEnterDetroDeConfirmaNovaSenha(){
        resetarSenha()
    }

    @Listen("onClick = #btnResetarSenha")
    def resetarSenha(){
        def buffet = validarSenhas()
        buffetService.resetarSenha(buffet, compNovaSenha.value)
        redirect(uri:"/login.zul")
    }

    def validarSenhas(){
        if(!compSenhaAtual.value) {
            compSenhaAtual.focus()
            alertarPorComponent(compSenhaAtual)
        }

        if(!compNovaSenha.value) {
            println "A ${compNovaSenha.value} ${!compNovaSenha.value}"
            compNovaSenha.focus()
            alertarPorComponent(compNovaSenha)
        }

        if(!compConfirmaNovaSenha.value) {
            compConfirmaNovaSenha.focus()
            alertarPorComponent(compConfirmaNovaSenha)
        }

        Buffet buffet = Buffet.findById(param.id)
        boolean mesmaSenha = buffet.usuario.senha.equals(compSenhaAtual.value.encodeAsSHA256())
        if(!mesmaSenha) throw new WrongValueException(compSenhaAtual, "Senha incorreta")

        if(!compNovaSenha.value.equals(compConfirmaNovaSenha.value)) {
            compNovaSenha.focus()
            compNovaSenha.value = ""
            compConfirmaNovaSenha.value = ""
            throw new WrongValueException(compNovaSenha, "Você inseriu senhas diferentes")
        }

        buffet
    }

}

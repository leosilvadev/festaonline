class UrlMappings {

    static excludes = ["/css/*", "/ext/*", "/images/*", "/js/*"]

	static mappings = {
        "/"(uri: "/buffet/listar.zul")
        "500"(view:'/error')
	}
}

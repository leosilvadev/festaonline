import entidades.Usuario
import enums.TipoUsuario

class BootStrap {

    def init = { servletContext ->
        String senha = "admin".encodeAsSHA256()
        Usuario usuario = Usuario.findByEmail("admin@admin.com") ?: new Usuario(email: "admin@admin.com", senha: senha, tipoUsuario: TipoUsuario.ADMIN).save()

    }
    def destroy = {
    }
}

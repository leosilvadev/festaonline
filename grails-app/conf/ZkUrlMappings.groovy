class ZkUrlMappings {

    static excludes = ["/css/*", "/ext/*", "/images/*", "/js/*"]

    static mappings = {
        "/"(view: "/login.zul")
        "/css"(resource: "/css")
    }

}

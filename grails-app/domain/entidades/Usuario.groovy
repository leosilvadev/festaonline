package entidades

import enums.TipoUsuario

class Usuario {

    String nome
    String email
    String senha
    TipoUsuario tipoUsuario
    String telefone

    static hasOne = [buffet:Buffet]

    static hasMany = [festas:Festa]

    static constraints = {
        email nullable: false, blank: false, email: true
        senha nullable: false, blank: false
        tipoUsuario nullable: false
        buffet nullable: true
        telefone nullable: true
        festas nullable: true
        nome nullable: true, blank: true
    }

    static mapping = {
        tipoUsuario enumType: 'ordinal'
    }
}

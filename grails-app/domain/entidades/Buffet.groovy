package entidades

class Buffet {

    String nome
    String endereco
    String telefone

    static hasMany = [festas:Festa]

    static belongsTo = [cidade:Cidade, usuario:Usuario]

    static constraints = {
        nome nullable: false, blank: false
        endereco nullable: false, blank: false
        cidade nullable: true
        usuario nullable: true
    }
}

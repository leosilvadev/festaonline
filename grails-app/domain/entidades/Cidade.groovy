package entidades

class Cidade {

    String nome

    static constraints = {
        nome blank: false
    }
}

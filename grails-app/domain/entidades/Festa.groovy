package entidades

class Festa {

    Date datahora
    String aniversariante

    static belongsTo = [buffet:Buffet, cliente: Usuario]

    static constraints = {
        aniversariante nullable: true, blank: true
    }
}

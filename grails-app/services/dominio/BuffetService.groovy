package dominio

import entidades.Buffet
import entidades.Usuario
import enums.TipoUsuario
import excecoes.ValidacaoException
import grails.transaction.Transactional
import groovy.xml.MarkupBuilder
import org.springframework.transaction.annotation.Propagation

@Transactional
class BuffetService {

    def mailService
    def geradorDeSenhaService

    def salvar(buffet, usuario, urlAplicacao) {
        usuario.senha = geradorDeSenhaService.gerarSenha(10)
        buffet.usuario = usuario

        if(buffet.usuario.validate()){
            buffet.usuario.save()

            String senha = buffet.usuario.senha
            buffet.usuario.senha = senha.encodeAsSHA256()

            if( buffet.validate() ){
                buffet.save()

                String urlLogin = "${urlAplicacao}/resetarSenha.zul?id=${buffet.id}"
                mailService.sendMail {
                    async true
                    to usuario.email
                    subject "FestaOnline - Cadastro de Buffet"
                    html gerarHtml(urlLogin, senha)
                }

            }else{
                throw new ValidacaoException(buffet.errors.fieldError)

            }
        }else{
            throw new ValidacaoException(buffet.usuario.errors.fieldError)

        }
    }

    def resetarSenha(Buffet buffet, String novaSenha){
        Usuario usuario = buffet.usuario
        usuario.senha = novaSenha.encodeAsSHA256()
        usuario.save()
    }

    def atualizar(Buffet buffet) {
        Usuario usuario = buffet.usuario
        if(usuario.validate()){
            usuario.merge()

            if( buffet.validate() ){
                buffet.merge()

            }else{
                throw new ValidacaoException(buffet.errors.fieldError)

            }
        }else{
            throw new ValidacaoException(buffet.usuario.errors.fieldError)

        }
    }

    def remover(Long buffetId){
        Buffet buffet = Buffet.findById(buffetId)
        Usuario usuario = buffet.usuario

        buffet.delete()
        usuario.delete()
    }

    def gerarHtml(urlLogin, senha){
        def sb = new StringWriter()
        new groovy.xml.MarkupBuilder(sb).html {
            body{
                div{
                    h1("Buffet cadastrado com sucesso!")
                }
                div{
                    h4("Bem vindo ao FestaOnline! Você pode acessar sua conta através do link:")
                    a(href: urlLogin, "Clique para entrar no FestaOnline")
                }
                div{
                    h3("Senha: ${senha}")
                }
            }
        }
        return sb.toString()
    }

}

package dominio

import entidades.Buffet
import entidades.Festa
import entidades.Usuario
import excecoes.ValidacaoException
import grails.transaction.Transactional

@Transactional
class FestaService {

    def mailService
    def geradorDeSenhaService

    def salvar(Festa festa, Usuario usuario, urlAplicacao) {
        festa.cliente = usuario
        String senha = geradorDeSenhaService.gerarSenha(5)
        festa.cliente.senha = senha.encodeAsSHA256()

        if(festa.cliente.validate()){
            festa.cliente.save()

            if( festa.validate() ){
                festa.save()

                String urlLogin = "${urlAplicacao}/login.zul"
                mailService.sendMail {
                    async true
                    to usuario.email
                    subject "FestaOnline - Cadastro da sua Festa!"
                    html gerarHtml(urlLogin, senha)
                }

            }else{
                throw new ValidacaoException(festa.errors.fieldError)

            }
        }else{
            throw new ValidacaoException(festa.cliente.errors.fieldError)

        }
    }

    def atualizar(Festa festa) {
        Usuario usuario = festa.cliente
        if(usuario.validate()){
            usuario.merge()

            if( festa.validate() ){
                festa.merge()

            }else{
                throw new ValidacaoException(festa.errors.fieldError)

            }
        }else{
            throw new ValidacaoException(festa.cliente.errors.fieldError)

        }
    }

    def gerarHtml(urlLogin, codigoAcesso){
        def sb = new StringWriter()
        new groovy.xml.MarkupBuilder(sb).html {
            body{
                div{
                    h1("Sua festa foi cadastrada com sucesso!")
                }
                div{
                    h4("Bem vindo ao FestaOnline! Você pode acessar sua festa através do link:")
                    a(href: urlLogin, "Clique para entrar no FestaOnline")
                }
                div{
                    h3("Seu código de acesso é: ${codigoAcesso}")
                }
            }
        }
        return sb.toString()
    }
    def remover(festaId){
        Festa festa = Festa.findById(festaId)
        Usuario usuario = festa.cliente

        festa.delete()
        usuario.delete()
    }

}

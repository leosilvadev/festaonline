package dominio

import grails.transaction.Transactional
import org.apache.commons.lang.RandomStringUtils

@Transactional
class GeradorDeSenhaService {

    def gerarSenha(tamanho) {
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        RandomStringUtils.random(tamanho, charset.toCharArray())
    }
}

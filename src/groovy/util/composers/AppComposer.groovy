package util.composers

import entidades.Usuario
import org.springframework.context.i18n.LocaleContextHolder
import org.zkoss.zk.grails.composer.GrailsComposer
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.WrongValueException
import org.zkoss.zul.Messagebox
import seguranca.AutenticacaoAdmin

/**
 * Created by leonardo on 09/10/14.
 */
class AppComposer extends GrailsComposer {

    def usuarioLogado(){
        Usuario usuario = Executions.current.session.getAttribute(AutenticacaoAdmin.USUARIO_LOGADO)
        usuario
    }

    def urlAplicacao(){
        String porta = ( Executions.getCurrent().getServerPort() == 80 ) ? "" : (":" + Executions.getCurrent().getServerPort())
        String scheme = Executions.getCurrent().getScheme()
        String nomeServidor =  Executions.getCurrent().getServerName()
        String nomeAplicacao = Executions.getCurrent().getContextPath()
        "${scheme}://${nomeServidor}${porta}${nomeAplicacao}"
    }

    def confirmar(String mensagem, org.zkoss.zk.ui.event.EventListener event){
        Messagebox.show(
                mensagem,
                "Confirmação",
                Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                event);
    }

    def alertar(campoComErro){
        String nome = campoComErro.field
        throw new WrongValueException(this."comp${nome.capitalize()}", "Preencha corretamente")
    }

    def alertarPorComponent(componente){
        throw new WrongValueException(componente, "Preencha corretamente")
    }

}

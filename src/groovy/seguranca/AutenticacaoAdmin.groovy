package seguranca

import entidades.Usuario
import enums.TipoUsuario
import org.zkoss.zk.ui.Execution
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.Page
import org.zkoss.zk.ui.Session
import org.zkoss.zk.ui.Sessions
import org.zkoss.zk.ui.util.Initiator

import javax.servlet.http.HttpServletRequest
import java.util.regex.Matcher

/**
 * Created by leonardo on 16/10/14.
 */
class AutenticacaoAdmin implements Initiator {

    public static String USUARIO_LOGADO = "usuario_logado"

    void doInit(Page page, Map<String, Object> stringObjectMap) throws Exception {
        Usuario usuario = Executions.current.session.getAttribute(USUARIO_LOGADO)

        if(!usuario?.tipoUsuario?.equals(TipoUsuario.ADMIN)){
            Executions.sendRedirect("/login.zul");
            return;

        }
    }

}

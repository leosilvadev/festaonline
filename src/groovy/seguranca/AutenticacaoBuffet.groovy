package seguranca

import entidades.Usuario
import enums.TipoUsuario
import org.zkoss.zk.ui.Execution
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.Page
import org.zkoss.zk.ui.util.Initiator

/**
 * Created by leonardo on 16/10/14.
 */
class AutenticacaoBuffet implements Initiator {

    public static String USUARIO_LOGADO = "usuario_logado"

    void doInit(Page page, Map<String, Object> stringObjectMap) throws Exception {
        Usuario usuario = Executions.current.session.getAttribute(USUARIO_LOGADO)

        if(!usuario?.tipoUsuario?.equals(TipoUsuario.BUFFET)){
            Executions.sendRedirect("/login.zul");
            return;

        }
    }

}

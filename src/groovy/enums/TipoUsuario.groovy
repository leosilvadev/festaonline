package enums

/**
 * Created by leonardo on 12/10/14.
 */
enum TipoUsuario {

    ADMIN("/buffet/listar.zul"),
    BUFFET("/buffet/home.zul"),
    CLIENT("/cliente/home.zul");

    private String paginaPrincipal

    private TipoUsuario(String paginaPrincipal){
        this.paginaPrincipal = paginaPrincipal
    }

    public String getPaginaPrincipal(){
        paginaPrincipal
    }

}
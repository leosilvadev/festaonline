package excecoes;

import org.springframework.validation.FieldError;

/**
 * Created by leonardo on 16/10/14.
 */
public class ValidacaoException extends RuntimeException {

    private FieldError fieldError;

    public ValidacaoException(FieldError fieldError){
        this.fieldError = fieldError;
    }

    public FieldError getFieldError(){
        return this.fieldError;
    }

}
